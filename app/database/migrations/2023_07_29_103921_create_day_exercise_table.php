<?php

use Illuminate\Database\Migrations\Migration;
use Illuminate\Database\Schema\Blueprint;
use Illuminate\Support\Facades\Schema;

return new class extends Migration
{
    /**
     * Run the migrations.
     */
    public function up(): void
    {
        Schema::create('day_exercise', function (Blueprint $table) {
            $table->id();
            $table->date('date');
            $table->foreignId('user_id')->constrained()->cascadeOnDelete(); // New field for user association
            $table->foreignId('exercise_id')->constrained()->cascadeOnDelete();
            $table->foreignId('routine_id')->nullable()->constrained()->nullOnDelete();
            $table->foreignId('superset_id')->nullable()->constrained()->nullOnDelete();
            $table->enum('type', ['Weight and Reps', 'Distance and Time', 'Weight and Distance', 'Weight and Time', 'Reps and Distance', 'Reps and Time', 'Weight', 'Reps', 'Distance', 'Time']);
            $table->string('distance')->nullable();
            $table->enum('distance_unit', ['m', 'km', 'ft', 'mi'])->nullable();
            $table->string('time')->nullable();  // This can be in the format HH:MM:SS
            $table->decimal('weight', 8, 2)->nullable();  // This allows up to 999,999.99 weight
            $table->enum('weight_unit', ['Metric (kgs)', 'Imperial (lbs)'])->nullable();
            $table->integer('reps')->nullable();  // This allows up to ~2 billion reps
            $table->timestamps();
        });
    }

    /**
     * Reverse the migrations.
     */
    public function down(): void
    {
        Schema::dropIfExists('day_exercise');
    }
};
