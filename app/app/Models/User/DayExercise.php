<?php

namespace App\Models\User;

use Illuminate\Database\Eloquent\Factories\HasFactory;
use Illuminate\Database\Eloquent\Model;

class DayExercise extends Model
{
    use HasFactory;
    protected $fillable = [
        'date',
        'exercise_id',
        'routine_id',
        'user_id', // New field
        'type',
        'distance',
        'distance_unit',
        'time',
        'weight',
        'weight_unit',
        'reps',
    ];
    public function user()
    {
        return $this->belongsTo(User::class);
    }
    public function exercise()
    {
        return $this->belongsTo(Exercise::class);
    }
    public function routine()
    {
        return $this->belongsTo(Routine::class);
    }
    public function superset()
    {
        return $this->belongsTo(Superset::class);
    }
}
