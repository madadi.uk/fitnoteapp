<?php

namespace App\Models\User;

use Illuminate\Database\Eloquent\Factories\HasFactory;
use Illuminate\Database\Eloquent\Model;

class Exercise extends Model
{
    use HasFactory;

    protected $fillable = ['name', 'notes', 'category_id', 'user_id'];

    public function category()
    {
        return $this->belongsTo(related: Category::class);
    }
    public function user()
    {
        return $this->belongsTo(related: User::class);
    }
    public function dayExercises()
    {
        return $this->hasMany(related: DayExercise::class);
    }
}
