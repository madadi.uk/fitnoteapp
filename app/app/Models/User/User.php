<?php

namespace App\Models\User;

use Illuminate\Contracts\Auth\MustVerifyEmail;
use Illuminate\Database\Eloquent\Factories\HasFactory;
use Illuminate\Foundation\Auth\User as Authenticatable;
use Illuminate\Notifications\Notifiable;
use Laravel\Sanctum\HasApiTokens;
use PHPOpenSourceSaver\JWTAuth\Contracts\JWTSubject;

class User extends Authenticatable implements JWTSubject
{
    use HasApiTokens, HasFactory, Notifiable;

    /**
     * The attributes that are mass assignable.
     *
     * @var array<int, string>
     */
    protected $fillable = [
            //| id           | bigint unsigned | NO   | PRI | NULL    | auto_increment |
            'mobile',     //| varchar(11)     | NO   | UNI | NULL     |                |
            'email',       //| varchar(255)    | NO   | UNI | NULL    |                |
            'password',    //| varchar(255)    | NO   |     | NULL    |                |
            //| created_at   | timestamp       | YES  |     | NULL    |                |
            //| updated_at   | timestamp       | YES  |     | NULL    |                |
    ];

    /**
     * The attributes that should be hidden for serialization.
     *
     * @var array<int, string>
     */
    protected $hidden = [
        'password',
        'remember_token',
    ];
    /**
     * The attributes that should be cast.
     *
     * @var array<string, string>
     */
    protected $casts = [
        'email_verified_at' => 'datetime',
    ];

    /**
     * The attributes that should be cast.
     *
     * @var array<string, string>
     */
    public function getJWTIdentifier()
    {
        return $this->getKey();
    }
    /**
     * Return a key value array, containing any custom claims to be added to the JWT.
     *
     * @return array
     */

    public function getJWTCustomClaims()
    {
        return [];
    }

    public function exercises()
    {
        return $this->hasMany(related: Exercise::class);
    }

    public function routines()
    {
        return $this->hasMany(related: Routine::class);
    }
    public function dayExercises()
    {
        return $this->hasMany(related: DayExercise::class);
    }

}
