<?php

namespace App\Models\User;

use Illuminate\Database\Eloquent\Factories\HasFactory;
use Illuminate\Database\Eloquent\Model;

class Routine extends Model
{
    use HasFactory;
    protected $fillable = ['name', 'note', 'user_id'];

    public function user()
    {
        return $this->belongsTo(User::class);
    }

    public function dayExercises()
    {
        return $this->hasMany(DayExercise::class);
    }
}
