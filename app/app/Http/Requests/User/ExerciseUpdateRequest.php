<?php

namespace App\Http\Requests\User;

use Illuminate\Foundation\Http\FormRequest;

class ExerciseUpdateRequest extends FormRequest
{
    /**
     * Determine if the user is authorized to make this request.
     */
    public function authorize(): bool
    {
        return false;
    }

    /**
     * Get the validation rules that apply to the request.
     *
     * @return array<string, \Illuminate\Contracts\Validation\ValidationRule|array|string>
     */
    public function rules(): array
    {
        return [
            'user_id' => 'required|exists:users,id',
            'category_id' => 'required|exists:categories,id',
            'name' => 'required|max:255',
            'notes' => 'nullable',
            'type' => 'required|in:Weight and Reps,Distance and Time,Weight and Distance,Weight and Time,Reps and Distance,Reps and Time,Weight,Reps,Distance,Time',
            'distance' => 'required_if:type,Distance and Time,Weight and Distance,Reps and Distance|nullable',
            'distance_unit' => 'required_with:distance|in:m,km,ft,mi|nullable',
            'time' => 'required_if:type,Distance and Time,Weight and Time,Reps and Time|nullable',
            'weight' => 'required_if:type,Weight and Reps,Weight and Distance,Weight and Time|nullable|numeric',
            'weight_unit' => 'required_with:weight|in:Metric (kgs),Imperial (lbs)|nullable',
            'reps' => 'required_if:type,Weight and Reps,Reps and Distance,Reps and Time|nullable|integer',
        ];
    }
}
