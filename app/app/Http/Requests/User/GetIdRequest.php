<?php

namespace App\Http\Requests\User;

use App\Http\Requests\FormRequestBase;
use App\Models\Campaign\Campaign;
use App\Rules\Campaign\CheckCampaignTimeRule;
use Illuminate\Contracts\Validation\ValidationRule;


class GetIdRequest extends FormRequestBase
{
    /**
     * Determine if the user is authorized to make this request.
     */
    public function authorize(): bool
    {
        return true;
    }

    /**
     * Get the validation rules that apply to the request.
     *
     * @return array<string, ValidationRule|array|string>
     */
    public function rules(): array
    {
        return [
            // We don't include 'id' here because it's not a field in the request body or query parameters.
            // You could include other fields here if you're validating them.
            'id' => 'required|integer|exists:users,id',
        ];
    }
}
