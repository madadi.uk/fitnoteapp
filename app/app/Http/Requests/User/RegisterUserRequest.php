<?php

namespace App\Http\Requests\User;

use App\Http\Requests\FormRequestBase;
use App\Models\User\User;
use App\Rules\Campaign\CheckCampaignTimeRule;
use Illuminate\Contracts\Validation\ValidationRule;


class RegisterUserRequest extends FormRequestBase
{
    /**
     * Determine if the user is authorized to make this request.
     */
    public function authorize(): bool
    {
        return true;
    }

    /**
     * Get the validation rules that apply to the request.
     *
     * @return array<string, ValidationRule|array|string>
     */
    public function rules(): array
    {

        return [
            'mobile' => ['required','min:11', 'max:11', 'unique:users,mobile'],
            'email' => ['required', 'email', 'unique:users,email'],
            'password' => ['required', 'min:8', 'max:255'],
        ];
    }
}
