<?php
namespace App\Http\Requests\User;

use Illuminate\Foundation\Http\FormRequest;

class CategoryStoreRequest extends FormRequest
{
    public function authorize()
    {
        return true;  // Or implement custom authorization logic
    }

    public function rules()
    {
        return [
            'title' => 'required|unique:categories|max:255',
        ];
    }
}
