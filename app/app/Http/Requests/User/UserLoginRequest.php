<?php

namespace App\Http\Requests\User;

use App\Http\Requests\FormRequestBase;
use App\Models\Campaign\Campaign;
use App\Rules\Campaign\CheckCampaignTimeRule;
use Illuminate\Contracts\Validation\ValidationRule;
use Illuminate\Validation\Rule;


class UserLoginRequest extends FormRequestBase
{
    /**
     * Determine if the user is authorized to make this request.
     */
    public function authorize(): bool
    {
        return true;
    }

    /**
     * Get the validation rules that apply to the request.
     *
     * @return array<string, ValidationRule|array|string>
     */
    public function rules(): array
    {
        return [
            'email' => ['required', 'email'],
            'password' => ['required', 'min:8', 'max:255'],
        ];
    }
}
