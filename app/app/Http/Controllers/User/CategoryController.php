<?php
namespace App\Http\Controllers\User;
use App\Http\Controllers\Controller;
use App\Http\Requests\User\CategoryStoreRequest;
use App\Http\Requests\User\ExerciseStoreRequest;
use App\Http\Requests\User\ExerciseUpdateRequest;
use App\Models\User\Category;
use App\Models\User\Exercise;

class CategoryController extends Controller
{
    public function store(categoryStoreRequest $request)
    {
        $category = Category::create($request->validated());
        return response()->json($category, 201);
    }

}

