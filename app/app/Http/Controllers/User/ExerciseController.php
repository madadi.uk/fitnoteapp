<?php
namespace App\Http\Controllers\User;
use App\Http\Controllers\Controller;
use App\Http\Requests\User\ExerciseStoreRequest;
use App\Http\Requests\User\ExerciseUpdateRequest;
use App\Models\User\Exercise;

class ExerciseController extends Controller
{
    public function store(ExerciseStoreRequest $request)
    {
        $exercise = Exercise::create($request->validated());
        return response()->json($exercise, 201);
    }
    public function update(ExerciseUpdateRequest $request, Exercise $exercise)
    {
        $exercise->update($request->validated());
        return response()->json($exercise, 200);
    }

}

