<?php

namespace App\Http\Controllers\User;

use App\Http\Controllers\Controller;
use App\Http\Requests\User\SearchByEmailRequest;
use App\Http\Requests\User\SearchByMobileRequest;
use App\Http\Requests\User\UpdateUserRequest ;
use App\Http\Requests\User\DeleteRequest;
use App\Http\Requests\User\GetIdRequest;
use App\Http\Requests\User\AllUserRequest;
use App\Models\User\User;
use Illuminate\Support\Facades\Hash;

class Users extends Controller
{
//        public function register(RegisterUserRequest $request)
//        {
//
//            $user = User::where('mobile', $request->mobile)
//                ->orwhere('email', $request->email)
//                ->first();
//            if ($user) {
//                return response()->json([
//                    'status' => 'error',
//                    'message' => 'A user with this mobile number or email already exists.'
//                ], 400);
//            }
//            $user = User::create([
//                'mobile' => $request->mobile,
//                'email' => $request->email,
//                'password' => Hash::make($request->password),
//            ]);
//            return response()->json([
//                'status' => 'success',
//                'data' => $user
//            ], 200);
//        }
        public function get(GetIdRequest $request, $id)
        {
            // Retrieve the authenticated user
            $authUser = auth()->user();

            // Retrieve the user by ID
            $user = User::find($id);

            // Check if the user exists
            if (!$user) {
                return response()->json([
                    'status' => 'error',
                    'message' => 'user not found'
                ], 404);
            }
            if ($authUser->id != $user->id) {
                return response()->json([
                    'status' => 'error',
                    'message' => 'You can only view your own account'
                ], 403);
            }
            return response()->json([
                'status' => 'success',
                'data' => $user
            ], 200);
        }

        public function update(UpdateUserRequest  $request, $id)
        {
            // Retrieve the authenticated user
            $user= auth()->user();
            // Check if the authenticated user is the same as the user being updated
            if ($user->id != $id) {
                return response()->json([
                    'status' => 'error',
                    'message' => 'You can only edit your own account'
                ], 403);
            }
            // Validate and update user fields
            // You can customize the fields to match your database schema
            $user->update([
                'mobile' => $request->mobile,
                'email' => $request->email,
                'password' => Hash::make($request->password),
                // Add other fields that are allowed to be updated
            ]);
            return response()->json([
                'status' => 'success',
                'message' => 'user updated successfully',
                'data' => $user
            ], 200);
        }

        public function delete(DeleteRequest $request, $id)
        {
            // Retrieve the authenticated user
            $authUser = auth()->user();
            // Check if the authenticated user is trying to delete their own account
            if ($authUser->id != $id) {
                return response()->json([
                    'status' => 'error',
                    'message' => 'You can only delete your own account'
                ], 403);
            }
            // Delete the user
            $authUser->delete();

            return response()->json([
                'status' => 'success',
                'message' => 'user deleted successfully'
            ], 200);

        }

        /* // later i will move this method to admin methods.

        public function all(AllUserRequest $request)
        {
            $page = $request->get('page');
            $limit = $request->get('limit');

            // Use your pagination logic here
            $users = User::paginate($limit, ['*'], 'page', $page);

            return response()->json([
                'status' => 'success',
                'data' => $users
            ], 200);
        }

    public function SearchByMobile(SearchByMobileRequest $request)
    {
        $searchTerm = $request->input('mobile');
        $users = User::where('mobile', 'LIKE', "%{$searchTerm}%")->get();

        return response()->json([
            'status' => 'success',
            'data' => $users
        ], 200);
    }
        */
    public function SearchByEmail(SearchByEmailRequest $request)
    {
        $authUser = auth()->user();

        // Get the email from the request
        $searchTerm = $request->input('email');

        // Check if the search email matches the authenticated user's email
        if ($searchTerm != $authUser->email) {
            return response()->json([
                'status' => 'error',
                'message' => 'Unauthorized'
            ], 403);
        }

        $users = User::where('email', 'LIKE', "%{$searchTerm}%")->get();

        return response()->json([
            'status' => 'success',
            'data' => $users
        ], 200);
    }

}
