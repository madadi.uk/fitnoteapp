<?php

namespace App\Http\Controllers\User;

use App\Http\Controllers\Controller;
use App\Http\Requests\UserSuperset\StoreSupersetRequest;
use App\Models\User\Superset;

class SupersetController extends Controller
{
    public function store(StoreSupersetRequest $request)
    {
        $superset = new Superset();
        $superset->user_id = $request->user()->id;
        $superset->name = $request->input('name');
        $superset->save();

        return response()->json([
            'message' => 'Superset created successfully',
            'superset' => $superset,
        ]);
    }
}
