<?php

namespace App\Http\Controllers\Api;

use App\Http\Controllers\Controller;
use App\Http\Requests\Api\Auth\Login2Request;
use App\Http\Requests\Api\Auth\LoginRequest;
use App\Models\User\User;


class AuthController extends Controller
{

    public function register(LoginRequest $request)
    {

       $user = User::where('mobile', $request->mobile)->first();
       if ($user) {
           return response()->json([
               'status' => 'error',
               'message' => 'user already exists'
           ], 400);
       }
         $user = User::create([
              'mobile' => $request->mobile,
         ]);
            return response()->json([
                'status' => 'success',
                'data' => $user
            ], 200);
    }

    public function login(Login2Request $request)
    {

        $user = User::where('mobile', $request->mobile)->first();
        if (!$user) {
            return response()->json([
                'status' => 'error',
                'message' => 'user not found'
            ], 404);
        }

        auth('api')->loginUsingId($user->id);

        return response()->json([
            'status' => 'success',
            'data' => $user
        ], 200);
    }


}
