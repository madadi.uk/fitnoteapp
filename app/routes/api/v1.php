<?php

use App\Http\Controllers\Api\Campaign\ApiCampaignController;
use App\Http\Controllers\Api\Wallet\WalletController;
use Illuminate\Support\Facades\Route;

/*
|--------------------------------------------------------------------------
| API Routes
|--------------------------------------------------------------------------
|
| Here is where you can register API routes for your application. These
| routes are loaded by the RouteServiceProvider and all of them will
| be assigned to the "api" middleware group. Make something great!
|
*/
Route::post('register', 'App\Http\Controllers\User\AuthController@register');
Route::post('logout', 'App\Http\Controllers\User\AuthController@logout')->middleware('auth:api');
Route::post('refresh', 'App\Http\Controllers\User\AuthController@refresh')->middleware('auth:api');
Route::post('login', 'App\Http\Controllers\User\AuthController@login');

Route::prefix('users')->group(function() {
    Route::post('category', [\App\Http\Controllers\User\CategoryController::class, 'store']);
    Route::post('exercise', [\App\Http\Controllers\User\ExerciseController::class, 'store']);
    Route::patch('update/{id}', [\App\Http\Controllers\User\Users::class, 'update'])->middleware('auth:api');
    Route::get('get/{id}', [\App\Http\Controllers\User\Users::class, 'get'])->middleware('auth:api');
    Route::get('all', [\App\Http\Controllers\User\Users::class, 'all']);
    Route::delete('delete/{id}', [\App\Http\Controllers\User\Users::class, 'delete'])->middleware('auth:api');
    Route::get('searchByEmail', [\App\Http\Controllers\User\Users::class, 'SearchByEmail'])->middleware('auth:api');
//    Route::get('searchByMobile', [\App\Http\Controllers\User\Users::class, 'SearchByMobile'])->middleware('auth:api');
    Route::post('superset', [\App\Http\Controllers\User\SupersetController::class, 'store']);
});


Route::prefix( 'campaigns')
     ->controller( ApiCampaignController::class)
     ->group(function () {

         Route::post('participation', 'participationToCampaign');

});


Route::prefix( 'users')
     ->group(function () {



         Route::prefix( 'wallet')
             ->controller( WalletController::class)
              ->group(function () {

                  Route::get('balance/{mobile}', 'balance');
         });

});
