### About Fitnote App

Fitnote is a comprehensive fitness tracker, designed for enthusiasts and professionals alike. Users can log their exercises, manage categories, and create supersets tailored to their fitness routines. Beyond just a tracker, Fitnote offers insights, helping users optimize their workouts and track their progress over time.---

### Architecture

In this project, we used service, repository architecture.   
After receiving our requests in the controller, they are sent to the service to perform logical operations, and if the service needs to connect to the database through the repository layer, it receives information from the model and database and then returns the result to the controller.

---



#### Tools used:   
Laravel 10  
MySQL 8
React 18
Redis 7  
Nginx  
Docker 


### How to run project:

For build project run follow command:
``` 
docker-compose build
```

Then enter the following command to run the project
``` 
docker-compose up -d
```

Now run the following commands in the terminal to receive packages and perform database migrations:

``` 
docker exec -it challenge-app composer install
```

``` 
docker exec -it challenge-app php artisan migrate
```

---

Now Project is running!  

For see the project home page open http://localhost:8085

---
#### postman collection
``` 
https://www.postman.com/gold-satellite-62889/workspace/fitnote-app/collection/22829853-5705ad84-daff-46e9-aefb-9e427fb9c959?action=share&creator=22829853```

---
#### Project charts
Also you can see all charts in `charts` directory

[charts](charts)


       
